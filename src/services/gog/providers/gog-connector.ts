import * as https from 'https';
import { IncomingMessage } from 'http';
import * as querystring from 'querystring';

import { HttpRequestOptions } from '../../shared/connector';

/**
 * Parameters for the detail request
 */
const detailHttpRequestOptions = {
    hostname: 'embed.gog.com',
    port: 443,
    method: 'GET',
    path: '/game/',
    headers: {
        'Content-Type': 'text/html; charset=UTF-8',
        'accept': '*/*'
    }
}

/**
 * Parameters for the search request
 */
const searchHttpRequestOptions = {
    hostname: 'embed.gog.com',
    port: 443,
    method: 'GET',
    path: '/games/ajax/filtered?',
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'accept': '*/*'
    }
}

/**
 * Class in charge of doing the net requests
 */
export class GOGConnector {
    private requestOptions: HttpRequestOptions;

    constructor() {
        this.requestOptions = new HttpRequestOptions();
    }

    /**
     * Write the data chunks into a single string
     * @param res HTTPS response
     * @param resolve Function to finish
     */
    static async writeDataChunk(res: IncomingMessage, resolve) {

        if (res.statusCode == 200) {
            res.setEncoding('utf8');
            let rawData = '';
            res.on('data', (chunk) => { rawData += chunk; });
            res.on('end', () => {
                try {
                    resolve(rawData);
                } catch (e) {
                    console.error(e.message);
                }
            });
        }
    }

    /**
     * Set the request options and do the connection for details
     * @param id App ID of the target
     * @param language Language for the results
     * @param currency Currency for the results
     */
    async detail(id: string): Promise<string> {
        this.requestOptions.setOptions(detailHttpRequestOptions);
        this.requestOptions.$path = id;
        let result: Promise<string> = new Promise<string>((resolve, reject) => {
            https.get(this.requestOptions.getOptionsObject(), res => {
                GOGConnector.writeDataChunk(res, resolve);
            })
        });

        return result;
    }

    /**
     * Set the request options and do the connection for search
     * @param gameName Game name to do the search
     * @param language Language for the results
     * @param currency Currency for the results
     */
    async search(gameName: string): Promise<string> {
        this.requestOptions.setOptions(searchHttpRequestOptions);
        this.requestOptions.$path = querystring.stringify({ mediaType: 'game', search: gameName, sort: 'popularity' })
        let result: Promise<string> = new Promise<string>((resolve, reject) => {
            https.get(this.requestOptions.getOptionsObject(), res => {
                GOGConnector.writeDataChunk(res, resolve);
            })
        });

        return result;
    }
}
