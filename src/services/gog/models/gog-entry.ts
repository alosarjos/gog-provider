/**
 * Class for the info for each search result
 */
export class GOGSearchEntry {
    private id: string;
    private name: string;
    private url: string;
    private price: string;
    private imageUrl: string;

    constructor($id: string, $name: string, $url: string, $price: string, $imageUrl: string) {
        this.id = $id;
        this.name = $name;
        this.url = $url;
        this.price = $price;
        this.imageUrl = $imageUrl
    }

    public get $id(): string {
        return this.id;
    }

    public get $name(): string {
        return this.name;
    }

    public get $url(): string {
        return this.url;
    }

    public get $price(): string {
        return this.price;
    }

    public get $imageUrl(): string {
        return this.imageUrl;
    }
}

/**
 * Class for the details  of each game
 */
export class GOGDetailEntry {
    private id: string;
    private name: string;
    private priceData: PriceInfo;
    private otherData: OtherInfo;

    constructor($id: string, $name: string, $priceData: PriceInfo, $otherData: OtherInfo) {
        this.id = $id;
        this.name = $name;
        this.priceData = $priceData;
        this.otherData = $otherData;
    }

    public get $id(): string {
        return this.id;
    }

    public get $name(): string {
        return this.name;
    }

    public get $priceData(): PriceInfo {
        return this.priceData;
    }

    public get $otherData(): OtherInfo {
        return this.otherData;
    }
}

/**
 * Other info inside SteamDetailEntry
 */
export class OtherInfo {
    private imageUrl: string;
    private platforms: Array<string>;
    private languages: string;
    private features: Array<string>;
    private developer: string;
    private publisher: string;

    constructor($imageUrl: string, $platforms: Array<string>, $languages: string, $features: Array<string>, $developer: string, $publisher: string) {
        this.imageUrl = $imageUrl;
        this.platforms = $platforms;
        this.languages = $languages;
        this.features = $features;
        this.developer = $developer;
        this.publisher = $publisher;
    }

    public get $imageUrl(): string {
        return this.imageUrl;
    }

    public get $platforms(): Array<string> {
        return this.platforms;
    }

    public get $languages(): string {
        return this.languages;
    }

    public get $features(): Array<string> {
        return this.features;
    }

    public get $developer(): string {
        return this.developer;
    }

    public get $publisher(): string {
        return this.publisher;
    }

}

/**
 * Pricing info inside SteamDetailEntry
 */
export class PriceInfo {
    private initialPrice: string;
    private finalPrice: string;
    private discountPercent: number;

    constructor($initialPrice: string, $finalPrice: string, $discountPercent: number) {
        this.initialPrice = $initialPrice;
        this.finalPrice = $finalPrice;
        this.discountPercent = $discountPercent;
    }

    public get $initialPrice(): string {
        return this.initialPrice;
    }

    public get $finalPrice(): string {
        return this.finalPrice;
    }

    public get $discountPercent(): number {
        return this.discountPercent;
    }
}



