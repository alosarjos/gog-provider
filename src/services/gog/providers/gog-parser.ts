import * as cheerio from 'cheerio';
import { GOGConnector } from './gog-connector';
import { GOGSearchEntry, GOGDetailEntry, OtherInfo, PriceInfo } from '../models/gog-entry'
import { GOGProvider } from './gog-provider';

/**
 * Data parsers class
 */
export class GOGParser {

    /**
     * Details data parser
     * @param jsonString JSON in string format to parse
     * @param id App ID of the game
     */
    static parseDetails(htmlString: string, id: string): GOGDetailEntry {
        let gameName = '';
        let imageUrl = '';
        let developer = '';
        let publisher = '';
        let languages = '';
        let platforms = new Array<string>();
        let features = new Array<string>();
        let initialPrice = '';
        let finalPrice = '';
        let discountPercent = 0;


        const $ = cheerio.load(htmlString)

        gameName = $('.header__title').text().trim();

        imageUrl = 'https:' + $('.productcard-wrapper.product--game').children().eq(1).attr('content')

        let details = $('.product-details');

        let rawPlatforms = details.children().eq(1).children().eq(1).text().trim()

        platforms = rawPlatforms.replace(/\(.*?\)/g, '').replace(' and', ',').split(',').map(string => {
            return string.trim();
        });

        languages = details.children().eq(2).children().eq(1).text().trim();

        developer = details.children().eq(4).children().eq(1).children().first().text().trim()
        publisher = details.children().eq(4).children().eq(1).children().last().text().trim()

        let rawFeatures = $('.game-features__item')

        rawFeatures.each(i => {
            features.push($('.game-features__item').eq(i).find('.game-features__title').text().trim())
        })

        if ($('.buy-price__save').length == 2) {
            //No deal
            initialPrice = finalPrice = $('.module-buy__info').children().eq(3).text().trim();

        } else if ($('.buy-price__save').length == 3) {
            //There is a deal
            initialPrice = $('.buy-price__old').text().replace(/{{(.*?)}}|[^0-9.]/g, '').trim()
            finalPrice = $('.buy-price__new').text().replace(/{{(.*?)}}|[^0-9.]/g, '').trim()
            discountPercent = Number($('.buy-price__save').eq(0).attr('data-discount').replace(/[^0-9]/g, '').trim())
        } else {
            //It's free
            initialPrice = finalPrice = '0.0'

        }
        return new GOGDetailEntry(id, gameName, new PriceInfo(initialPrice, finalPrice, discountPercent), new OtherInfo(imageUrl, platforms, languages, features, developer, publisher));
    }

    /**
     * Search data parser
     * @param html HTML website to parse
     */
    static parseSearch(jsonString: string): Array<GOGSearchEntry> {
        let results: Array<GOGSearchEntry> = new Array<GOGSearchEntry>();

        let id = '';
        let name = '';
        let url = '';
        let price = '';
        let imageUrl = '';

        let json = JSON.parse(jsonString);


        json.products.forEach(element => {
            id = element.slug.trim();
            name = element.title.trim();
            url = 'https://www.gog.com' + element.url;
            price = element.price.finalAmount;
            imageUrl = 'https://' + element.image;

            results.push(new GOGSearchEntry(id, name, url, price, imageUrl));
        });
        return results;
    }
}


