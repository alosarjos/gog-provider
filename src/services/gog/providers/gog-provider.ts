import { GOGSearchEntry, GOGDetailEntry } from '../models/gog-entry';
import { GOGConnector } from './gog-connector';
import { GOGParser } from './gog-parser'

/**
 * Class in charge of providing public methods
 */
export class GOGProvider {
    private gogConnector: GOGConnector;

    constructor() {
        this.gogConnector = new GOGConnector();
    }

    /**
     * Search through the Steam searching website. Results are limited to 10 for now.
     * @param gameName Name to search
     * @param language Language for the results (English by default)
     * @param currency Currency for the results (USD by default)
     */
    public async search(gameName: string): Promise<Array<GOGSearchEntry>> {
        let searchPage = await this.gogConnector.search(gameName);
        let result = GOGParser.parseSearch(searchPage);
        return result;
    }

    /**
     * 
     * @param gameId Steam AppId to retrieve it's detailts
     * @param language Language for the results (English by default)
     * @param currency Currency for the results (USD by default)
     */
    public async detail(gameId: string): Promise<GOGDetailEntry> {
        let detailRawData = await this.gogConnector.detail(gameId);
        let entry = GOGParser.parseDetails(detailRawData, gameId);
        return entry;
    }
}