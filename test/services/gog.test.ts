import { expect } from 'chai';

import { GOGSearchEntry, GOGDetailEntry } from '../../src/services/gog/models/gog-entry';
import { GOGProvider } from '../../src/services/gog/providers/gog-provider';

let provider = new GOGProvider();
let searchResult: Array<GOGSearchEntry>;
let detailResult: GOGDetailEntry;

describe('GOG provider search() method', () => {

    it('Should return a GOGSearchEntry array', () => {
        let result: Promise<Array<GOGSearchEntry>> = new Promise<Array<GOGSearchEntry>>((resolve, reject) => {
            provider.search('Witcher 3 Game of the year').then(result => {
                expect(result).to.be.an('array');
                expect(result).to.not.be.empty;
                expect(result).to.have.lengthOf(1);
                searchResult = result;
                resolve(result);
            });
        })
        return result;
    }).timeout(100000);


    it('Should have the Witcher 3: GOTY partial element in the array', () => {
        expect(searchResult[0].$id).to.equal('the_witcher_3_wild_hunt_game_of_the_year_edition');
        expect(searchResult[0].$name).to.equal('The Witcher 3: Wild Hunt - Game of the Year Edition');
        expect(searchResult[0].$price).to.equal('49.99');
        expect(searchResult[0].$url).to.equal('https://www.gog.com/game/the_witcher_3_wild_hunt_game_of_the_year_edition');
    })
});

describe('GOG provider detail() method', () => {
    it('Should return a GOGEntry element', () => {
        let result: Promise<GOGDetailEntry> = new Promise<GOGDetailEntry>((resolve, reject) => {
            provider.detail(searchResult[0].$id).then(result => {
                expect(result).not.to.be.empty;
                detailResult = result;
                resolve(result);
            });
        })
        return result;
    }).timeout(100000);

    it('Should be the Witcher 3: GOTY GOGEntry element', () => {
        expect(detailResult.$id).to.equal('the_witcher_3_wild_hunt_game_of_the_year_edition');
        expect(detailResult.$name).to.equal('The Witcher 3: Wild Hunt - Game of the Year Edition');
        expect(detailResult.$otherData.$developer).to.equal('CD PROJEKT RED');
        expect(detailResult.$otherData.$publisher).to.equal('CD PROJEKT RED');
        expect(detailResult.$priceData.$initialPrice).to.equal('49.99');
        expect(detailResult.$priceData.$finalPrice).to.equal('49.99');
        expect(detailResult.$priceData.$discountPercent).to.equal(0);
        expect(detailResult.$otherData.$platforms).to.have.lengthOf(1);
        expect(detailResult.$otherData.$features).to.have.lengthOf(4);
    });
})
