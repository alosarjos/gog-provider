# GOG Provider Library

## Usage

### Install the dependency

```
npm install gog-provider 
```

### Import the library

#### TypeScript
```TypeScript
import { GOGProvider } from 'gog-provider';
```

#### JavaScript
```JavaScript
var gog = require('gog-provider')
```

### Search for a game (Limited to 10 first results)

#### TypeScript
```TypeScript
let provider = new GOGProvider();

provider.search("Witcher").then(result => {
    console.log(result);
})
```

#### JavaScript
```JavaScript
var provider = new gog.GOGProvider();

provider.search("Witcher").then(result => {
    console.log(result)
})
```

### Search Results
```JavaScript
[ GOGSearchEntry {
    id: 'the_witcher_3_wild_hunt_game_of_the_year_edition',
    name: 'Witcher 3: Wild Hunt, The - Game of the Year Edition',
    url: 'https://www.gog.com/game/the_witcher_3_wild_hunt_game_of_the_year_edition',
    price: '49.99',
    imageUrl: 'https:////images-3.gog.com/e34b4189894110990510f1334151555bd1c0ed45926c5c8cbc7e9b80aa6744d1' },
  GOGSearchEntry {
    id: 'the_witcher_3_wild_hunt_expansion_pass',
    name: 'Witcher 3: Wild Hunt, The - Expansion Pass',
    url: 'https://www.gog.com/game/the_witcher_3_wild_hunt_expansion_pass',
    price: '24.99',
    imageUrl: 'https:////images-3.gog.com/110067209949b038a775d031c40226af85b2ea9b6b08a682ce46e473b6f09bf2' },
  GOGSearchEntry {
    id: 'the_witcher_2',
    name: 'Witcher 2: Assassins Of Kings, The - Enhanced Edition',
    url: 'https://www.gog.com/game/the_witcher_2',
    price: '3.59',
    imageUrl: 'https:////images-4.gog.com/8355e657a19311b158a3553a154e109199d6991c7791a20c3305af1f84d15ed7' },
  GOGSearchEntry {
    id: 'witcher_3_wild_hunt_the_blood_and_wine_pack',
    name: 'Witcher 3: Wild Hunt, The - Blood and Wine',
    url: 'https://www.gog.com/game/witcher_3_wild_hunt_the_blood_and_wine_pack',
    price: '19.99',
    imageUrl: 'https:////images-1.gog.com/b75a14ee368898610550591cdef7990b7ea3484fb44007ecd9904e03e8aa3f18' },
  GOGSearchEntry {
    id: 'the_witcher',
    name: 'Witcher: Enhanced Edition, The',
    url: 'https://www.gog.com/game/the_witcher',
    price: '8.89',
    imageUrl: 'https:////images-1.gog.com/37d4a208d1f5bb0e163da540ac894ba46a7d566ede31aaaefc74bbcd46ebd190' },
  GOGSearchEntry {
    id: 'the_witcher_3_wild_hunt_hearts_of_stone',
    name: 'Witcher 3: Wild Hunt, The - Hearts of Stone',
    url: 'https://www.gog.com/game/the_witcher_3_wild_hunt_hearts_of_stone',
    price: '9.99',
    imageUrl: 'https:////images-2.gog.com/91048944f471de09bd505bab2118a0c6f46d8f228fa5fe2662f4b50e79c42b23' },
  GOGSearchEntry {
    id: 'the_witcher_3_wild_hunt',
    name: 'Witcher 3: Wild Hunt, The',
    url: 'https://www.gog.com/game/the_witcher_3_wild_hunt',
    price: '29.99',
    imageUrl: 'https:////images-3.gog.com/60c724a052275a049d857d53957dc38e9347742f52372bb956d992b43efa8fb5' },
  GOGSearchEntry {
    id: 'witcher_adventure_game',
    name: 'Witcher Adventure Game, The',
    url: 'https://www.gog.com/game/witcher_adventure_game',
    price: '9.99',
    imageUrl: 'https:////images-2.gog.com/5bc58089e14771de0c1b872c529c8b8b150b87fe28c7e4db1856389526849bb3' },
  GOGSearchEntry {
    id: 'gwent_the_witcher_card_game',
    name: 'GWENT: The Witcher Card Game',
    url: 'https://www.gog.com/game/gwent_the_witcher_card_game',
    price: '0.00',
    imageUrl: 'https:////images-2.gog.com/e8e4fa69db4d06ff58b7ef8bda2ea72e966f419baf2e248807520cda6244b7d8' },
  GOGSearchEntry {
    id: 'the_witcher_3_wild_hunt_free_dlc_program',
    name: 'Witcher 3: Wild Hunt, The - Free DLC Program',
    url: 'https://www.gog.com/game/the_witcher_3_wild_hunt_free_dlc_program',
    price: '0.00',
    imageUrl: 'https:////images-1.gog.com/6638cec393d8936ffca6567d62db919ee0f82e218c5c96494f51e2544d531968' } ]
```

### Get game details

#### TypeScript
```TypeScript
let provider = new GOGProvider();

provider.detail("the_witcher_3_wild_hunt_game_of_the_year_edition").then(result => {
    console.log(result);
})
```

#### JavaScript
```JavaScript
var provider = new gog.GOGProvider();

provider.detail("the_witcher_3_wild_hunt_game_of_the_year_edition").then(result => {
    console.log(result)
})
```

### Detail Results
```JavaScript
GOGDetailEntry {
  id: 'the_witcher_3_wild_hunt_game_of_the_year_edition',
  name: 'Witcher 3: Wild Hunt, The - Game of the Year Edition',
  priceData:
   PriceInfo {
     initialPrice: '49.99',
     finalPrice: '49.99',
     discountPercent: 0 },
  otherData:
   OtherInfo {
     imageUrl: 'https://images-2.gog.com/e446c4533f6b360c9f9981b44947ac43f50483c04743152ed08b550425d05b6e_product_card_screenshot_748.jpg',
     platforms: [ 'Windows' ],
     languages: 'Audio and text: Deutsch, English, français, 日本語, polski, русский. Text only: العربية, Português do Brasil, 中文, český, español, Español (AL), magyar, italiano, 한국어',
     features:
      [ 'single-player',
        'achievements',
        'controller support',
        'cloud saves' ],
     developer: 'CD PROJEKT RED',
     publisher: 'CD PROJEKT RED' } }
```

## Notes
By default the library will recover data in US English and will get the prices in USD currency.

## Known Issues

## About & Credits

[gog-provider](https://gitlab.com/alosarjos/gog-provider) provides basic functionality for parsing GOG games information.

Credits to: [ckatzorke](https://github.com/ckatzorke/howlongtobeat) for providing me an excelent starting point for this project
